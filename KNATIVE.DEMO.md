## Create a service

```bash 
export KUBECONFIG=../../config/k3s.yaml
cd services/hello
# create a demo namespace
#kubectl create namespace demo

export KUBE_NAMESPACE="demo"
kubectl create namespace ${KUBE_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

```

### Login to the GitLab container registry
```bash
docker login registry.gitlab.com -u k33g -p ${GITLAB_TOKEN_ADMIN}
# echo "${GITLAB_TOKEN_ADMIN}" | docker login --username k33g --password-stdin
```

### Build and push the image

```bash
# build and push the image of the application
docker build -t registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/awesome-web-app .
# (🖐 don't forget the `.` at the end of the command)

docker push registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/awesome-web-app
```

### 🚀 deploy the service

```bash
kn service create one-service \
--namespace demo \
--env MESSAGE="I 💚 Knative" \
--env BACKGROUND_COLOR="mediumslateblue" \
--image registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/awesome-web-app \
--force
```

Do it again with some other values

```bash
kn service create one-service \
--namespace demo \
--env MESSAGE="I 💜 Knative a lot 😍" \
--env BACKGROUND_COLOR="yellow" \
--image registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/awesome-web-app \
--force
```

```bash
kn service create one-service \
--namespace demo \
--env MESSAGE="I 💙 Knative" \
--env BACKGROUND_COLOR="orange" \
--image registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/awesome-web-app \
--force
```
