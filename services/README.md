

## Atmo microservices

```bash
subo create project demo-atmo
cd demo-atmo
rm -rf .git
# update the source code

# build
subo build .
```

```bash
docker login registry.gitlab.com -u k33g -p ${GITLAB_TOKEN_ADMIN}
docker build -t registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/atmo-helloworld .
docker push registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/atmo-helloworld
```

```bash
kn service create atmo-service \
--namespace demo \
--env ATMO_HTTP_PORT="8080" \
--image registry.gitlab.com/k33g_org/gitlab-cookbook/kubernetes/knative-on-civo-cluster/atmo-helloworld \
--force
```


curl http://atmo-service.demo.212.2.240.213.sslip.io/hello -d 'Jane'

